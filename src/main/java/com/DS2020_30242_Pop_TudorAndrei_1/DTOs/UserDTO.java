package com.DS2020_30242_Pop_TudorAndrei_1.DTOs;

import com.DS2020_30242_Pop_TudorAndrei_1.Entity.UserRole;
import org.springframework.hateoas.RepresentationModel;

import java.util.Objects;


public class UserDTO extends RepresentationModel<UserDTO> {

    private Long id;
    private String userName;
    private String password;
    private UserRole role;

    public UserDTO() {
    }

    public UserDTO(Long id, String userName, String password, UserRole role) {
        this.id = id;
        this.userName = userName;
        this.password = password;
        this.role = role;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        UserDTO userDTO = (UserDTO) o;
        return id.equals(userDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), id);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }
}
