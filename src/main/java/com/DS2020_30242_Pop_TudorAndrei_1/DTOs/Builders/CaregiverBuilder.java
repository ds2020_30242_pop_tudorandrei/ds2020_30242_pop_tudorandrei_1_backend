package com.DS2020_30242_Pop_TudorAndrei_1.DTOs.Builders;

import com.DS2020_30242_Pop_TudorAndrei_1.Entity.Caregiver;
import com.DS2020_30242_Pop_TudorAndrei_1.Entity.Patient;
import com.DS2020_30242_Pop_TudorAndrei_1.Entity.User;

import java.util.ArrayList;

public class CaregiverBuilder {
    public static Caregiver getEmptyCaregiverAccout(User user) {
        return new Caregiver(
                user,
                null,
                new ArrayList<Patient>()
        );
    }
}
