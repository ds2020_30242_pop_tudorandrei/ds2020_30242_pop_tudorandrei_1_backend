package com.DS2020_30242_Pop_TudorAndrei_1.DTOs.Builders;

import com.DS2020_30242_Pop_TudorAndrei_1.Entity.Illness;
import com.DS2020_30242_Pop_TudorAndrei_1.Entity.MedicationPlan;
import com.DS2020_30242_Pop_TudorAndrei_1.Entity.Patient;
import com.DS2020_30242_Pop_TudorAndrei_1.Entity.User;

import java.util.ArrayList;
import java.util.Set;
import java.util.TreeSet;

public class PatientBuilder{

    private PatientBuilder() {
    }

    public static Patient getEmptyPatientAccount(User user){
        return new Patient(
                user,
                null,
                null,
                new ArrayList<MedicationPlan>(),
                new TreeSet<Illness>()
        );
    }
}

