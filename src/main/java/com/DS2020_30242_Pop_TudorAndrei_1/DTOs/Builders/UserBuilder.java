package com.DS2020_30242_Pop_TudorAndrei_1.DTOs.Builders;

import com.DS2020_30242_Pop_TudorAndrei_1.DTOs.UserDTO;
import com.DS2020_30242_Pop_TudorAndrei_1.DTOs.UserDetailsDTO;
import com.DS2020_30242_Pop_TudorAndrei_1.Entity.User;

public class UserBuilder {

    private UserBuilder(){

    }

    public static UserDTO toUserDTO(User user){
        return new UserDTO(user.getUserId(), user.getUserName(), user.getUserPassword(), user.getUserRole());
    }

    public static UserDetailsDTO toUserDetailsDTO(User user){
        return new UserDetailsDTO(user.getUserId(), user.getUserFirstName(), user.getUserLastName(), user.getUserBirthDate(), user.getUserAddress(), user.getUserGender());
    }

    public static User toEntity(UserDTO userDTO, UserDetailsDTO userDetailsDTO){

        return new User(
                userDTO.getUserName(),
                userDTO.getPassword(),
                userDetailsDTO.getFirstName(),
                userDetailsDTO.getLastName(),
                userDetailsDTO.getBirthDate(),
                userDetailsDTO.getAddress(),
                userDetailsDTO.getGender(),
                userDTO.getRole()
        );

    }




}
