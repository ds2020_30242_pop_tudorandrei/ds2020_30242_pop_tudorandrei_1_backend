package com.DS2020_30242_Pop_TudorAndrei_1.DTOs.Builders;

import com.DS2020_30242_Pop_TudorAndrei_1.Entity.Caregiver;
import com.DS2020_30242_Pop_TudorAndrei_1.Entity.Doctor;
import com.DS2020_30242_Pop_TudorAndrei_1.Entity.Patient;
import com.DS2020_30242_Pop_TudorAndrei_1.Entity.User;

import java.util.ArrayList;
import java.util.TreeSet;

public class DoctorBuilder {
    public static Doctor getEmptyDoctorAccount(User user) {
        return new Doctor(
                user,
                new ArrayList<>(),
                new ArrayList<>()
        );
    }
}
