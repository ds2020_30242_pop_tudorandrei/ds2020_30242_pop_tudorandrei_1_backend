package com.DS2020_30242_Pop_TudorAndrei_1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ds202030242PopTudorAndrei1Application {

	public static void main(String[] args) {
		SpringApplication.run(Ds202030242PopTudorAndrei1Application.class, args);

	}

}
