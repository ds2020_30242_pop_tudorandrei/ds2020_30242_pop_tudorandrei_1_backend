package com.DS2020_30242_Pop_TudorAndrei_1.Entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "medicationPlanId")
public class MedicationPlan {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long medicationPlanId;

    @JsonFormat(shape= JsonFormat.Shape.STRING, pattern="dd/MM/yyyy")
    @Column
    private Date startDate;

    @JsonFormat(shape= JsonFormat.Shape.STRING, pattern="dd/MM/yyyy")
    @Column
    private Date endDate;

    @OneToMany(cascade = CascadeType.REMOVE, fetch = FetchType.EAGER, orphanRemoval = true)
    private List<PrescribedMedication> prescribedMedications;

    public MedicationPlan() {
    }

    public MedicationPlan(Date startDate, Date endDate, List<PrescribedMedication> prescribedMedications) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.prescribedMedications = prescribedMedications;
    }

    public MedicationPlan(Long medicationPlanId, Date startDate, Date endDate, List<PrescribedMedication> prescribedMedications) {
        this.medicationPlanId = medicationPlanId;
        this.startDate = startDate;
        this.endDate = endDate;
        this.prescribedMedications = prescribedMedications;
    }

    public void addPrescribedMedication(PrescribedMedication prescribedMedication){
        prescribedMedications.add(prescribedMedication);
    }


    public void removePrescribedMedication(PrescribedMedication prescribedMedication){
        prescribedMedications.remove(prescribedMedication);
    }

    public Long getMedicationPlanId() {
        return medicationPlanId;
    }

    public void setMedicationPlanId(Long medicationPlanId) {
        this.medicationPlanId = medicationPlanId;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }


    public List<PrescribedMedication> getPrescribedMedications() {
        return prescribedMedications;
    }

    public void setPrescribedMedications(List<PrescribedMedication> prescribedMedications) {
        this.prescribedMedications = prescribedMedications;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MedicationPlan that = (MedicationPlan) o;
        return medicationPlanId.equals(that.medicationPlanId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(medicationPlanId);
    }

    @Override
    public String toString() {
        return "MedicationPlan{" +
                "medicationPlanId=" + medicationPlanId +
                ", prescribedMedications=" + prescribedMedications +
                '}';
    }
}
