package com.DS2020_30242_Pop_TudorAndrei_1.Entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Objects;

@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "drugId")
public class Drug {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long drugId;

    @Column
    private String drugName;

    @Column
    private String drugDescription;

    @Column
    private String drugSideEffects;

    public Drug() {
    }

    public Drug(String drugName, String drugDescription, String drugSideEffects) {
        this.drugName = drugName;
        this.drugDescription = drugDescription;
        this.drugSideEffects = drugSideEffects;
    }


    public Drug(Long drugId, String drugName, String drugDescription, String drugSideEffects) {
        this.drugId = drugId;
        this.drugName = drugName;
        this.drugDescription = drugDescription;
        this.drugSideEffects = drugSideEffects;
    }


    public String getDrugSideEffects() {
        return drugSideEffects;
    }

    public void setDrugSideEffects(String drugSideEffects) {
        this.drugSideEffects = drugSideEffects;
    }

    public Long getDrugId() {
        return drugId;
    }

    public void setDrugId(Long drugId) {
        this.drugId = drugId;
    }

    public String getDrugName() {
        return drugName;
    }

    public void setDrugName(String drugName) {
        this.drugName = drugName;
    }

    public String getDrugDescription() {
        return drugDescription;
    }

    public void setDrugDescription(String drugDescription) {
        this.drugDescription = drugDescription;
    }

    @Override
    public String toString() {
        return "Drug{" +
                "drugId=" + drugId +
                ", drugName='" + drugName + '\'' +
                ", drugDescription='" + drugDescription + '\'' +
                ", drugSideEffects='" + drugSideEffects + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Drug drug = (Drug) o;
        return drugId.equals(drug.drugId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(drugId);
    }
}
