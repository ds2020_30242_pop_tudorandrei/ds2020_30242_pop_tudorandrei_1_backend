package com.DS2020_30242_Pop_TudorAndrei_1.Entity;


import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Objects;

@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "prescribedMedicationId")
public class PrescribedMedication {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long prescribedMedicationId;

    @Column
    private Integer dosage;

    @Column
    private String intakeMoment;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
    private Drug drug;

    public PrescribedMedication() {
    }

    public PrescribedMedication(Integer dosage, String intakeMoment, Drug drug) {
        this.dosage = dosage;
        this.intakeMoment = intakeMoment;
        this.drug = drug;
    }

    public PrescribedMedication(Long prescribedMedicationId, Integer dosage, String intakeMoment, Drug drug) {
        this.prescribedMedicationId = prescribedMedicationId;
        this.dosage = dosage;
        this.intakeMoment = intakeMoment;
        this.drug = drug;
    }

    @Override
    public String toString() {
        return "PrescribedMedication{" +
                "prescribedMedicationId=" + prescribedMedicationId +
                ", dosage=" + dosage +
                ", intakeMoment='" + intakeMoment + '\'' +
                ", drug=" + drug.getDrugId() +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PrescribedMedication that = (PrescribedMedication) o;
        return prescribedMedicationId.equals(that.prescribedMedicationId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(prescribedMedicationId);
    }

    public Long getPrescribedMedicationId() {
        return prescribedMedicationId;
    }

    public void setPrescribedMedicationId(Long prescribedMedicationId) {
        this.prescribedMedicationId = prescribedMedicationId;
    }

    public Integer getDosage() {
        return dosage;
    }

    public void setDosage(Integer dosage) {
        this.dosage = dosage;
    }

    public String getIntakeMoment() {
        return intakeMoment;
    }

    public void setIntakeMoment(String intakeMoment) {
        this.intakeMoment = intakeMoment;
    }

    public Drug getDrug() {
        return drug;
    }

    public void setDrug(Drug drug) {
        this.drug = drug;
    }
}
