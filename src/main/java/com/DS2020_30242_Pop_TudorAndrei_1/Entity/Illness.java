package com.DS2020_30242_Pop_TudorAndrei_1.Entity;


import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "illnessId")
public class Illness {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long illnessId;

    @Column
    private String name;

    @Column
    private boolean chronic;

    @ManyToMany(cascade = CascadeType.ALL, mappedBy = "medicalRecord")
    private Set<Patient> affectedPatients;

    public Illness() {
    }

    public Illness(Long illnessId, String name, boolean chronic, Set<Patient> affectedPatients) {
        this.illnessId = illnessId;
        this.name = name;
        this.chronic = chronic;
        this.affectedPatients = affectedPatients;
    }

    public Long getIllnessId() {
        return illnessId;
    }

    public void setIllnessId(Long illnessId) {
        this.illnessId = illnessId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isChronic() {
        return chronic;
    }

    public void setChronic(boolean chronic) {
        this.chronic = chronic;
    }

    public Set<Patient> getAffectedPatients() {
        return affectedPatients;
    }

    public void setAffectedPatients(Set<Patient> affectedPatients) {
        this.affectedPatients = affectedPatients;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Illness illness = (Illness) o;
        return illnessId.equals(illness.illnessId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(illnessId);
    }

    @Override
    public String toString() {
        return "Illness{" +
                "illnessId=" + illnessId +
                ", name='" + name + '\'' +
                ", chronic=" + chronic +
                '}';
    }

    public void addPatient(Patient patient) {
        affectedPatients.add(patient);
    }

    public void removePatient(Patient patient) {
        affectedPatients.remove(patient);
    }
}
