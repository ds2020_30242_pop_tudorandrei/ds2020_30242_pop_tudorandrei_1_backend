package com.DS2020_30242_Pop_TudorAndrei_1.Entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import java.util.List;
import java.util.Objects;

@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "caregiverId")
public class Caregiver {

    @Id
    private Long caregiverId;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
    @MapsId
    @PrimaryKeyJoinColumn
    private User user;

    @ManyToOne(cascade = CascadeType.REFRESH)
    @JoinColumn(name = "doctor_id")
    private Doctor doctor;

    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH, orphanRemoval = true)
    private List<Patient> patients;

    public Caregiver() {
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    public Caregiver(User user, Doctor doctor, List<Patient> patients) {
        this.user = user;
        this.doctor = doctor;
        this.patients = patients;
    }

    public Caregiver(Long caregiverId, User user, Doctor doctor, List<Patient> patients) {
        this.caregiverId = caregiverId;
        this.user = user;
        this.doctor = doctor;
        this.patients = patients;
    }

    public void addPatient(Patient patient){
        patients.add(patient);
        //patient.setCaregiver(this);
    }

    public void removePatient(Patient patient){
        patients.remove(patient);
        //patient.setCaregiver(null);
    }

    public Long getCaregiverId() {
        return caregiverId;
    }

    public void setCaregiverId(Long caregiverId) {
        this.caregiverId = caregiverId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Patient> getPatients() {
        return patients;
    }

    public void setPatients(List<Patient> patients) {
        this.patients = patients;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Caregiver caregiver = (Caregiver) o;
        return caregiverId.equals(caregiver.caregiverId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(caregiverId);
    }

    @Override
    public String toString() {
        return "Caregiver{" +
                "caregiverId=" + caregiverId +
                ", user=" + user +
                '}';
    }
}
