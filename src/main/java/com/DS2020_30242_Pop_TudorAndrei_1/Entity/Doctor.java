package com.DS2020_30242_Pop_TudorAndrei_1.Entity;


import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "doctorId")
public class Doctor {

    @Id
    private Long doctorId;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
    @PrimaryKeyJoinColumn
    @MapsId
    private User user;

    @OneToMany(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER, orphanRemoval = true)
    private List<Patient> patients;

    @OneToMany(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER, orphanRemoval = true)
    private List<Caregiver> caregivers;


    public Doctor() {
    }

    public Doctor(Long doctorId, User user, List<Patient> patients, List<Caregiver> caregivers) {
        this.doctorId = doctorId;
        this.user = user;
        this.patients = patients;
        this.caregivers = caregivers;
    }

    public Doctor(User user, List<Patient> patients, List<Caregiver> caregivers) {
        this.user = user;
        this.patients = patients;
        this.caregivers = caregivers;
    }

    public void addPatient(Patient patient){

        patients.add(patient);
        patient.setDoctor(this);
    }

    public void removePatient(Patient patient){
        patients.remove(patient);
        patient.setDoctor(null);
    }

    public void addCaregiver(Caregiver caregiver){
        caregivers.add(caregiver);
        caregiver.setDoctor(this);
    }

    public void removeCaregiver(Caregiver caregiver){
        caregivers.remove(caregiver);
        caregiver.setDoctor(null);
    }


    public Long getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(Long doctorId) {
        this.doctorId = doctorId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Patient> getPatients() {
        return patients;
    }

    public void setPatients(List<Patient> patients) {
        this.patients = patients;
    }

    public List<Caregiver> getCaregivers() {
        return caregivers;
    }

    public void setCaregivers(List<Caregiver> caregivers) {
        this.caregivers = caregivers;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Doctor doctor = (Doctor) o;
        return doctorId.equals(doctor.doctorId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(doctorId);
    }

    @Override
    public String toString() {
        return "Doctor{" +
                "doctorId=" + doctorId +
                ", user=" + user +
                '}';
    }
}
