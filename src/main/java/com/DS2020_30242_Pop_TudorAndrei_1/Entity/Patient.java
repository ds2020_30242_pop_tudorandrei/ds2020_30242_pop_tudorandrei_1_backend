package com.DS2020_30242_Pop_TudorAndrei_1.Entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "patientId")
public class Patient {

    @Id
    private Long patientId;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
    @MapsId
    @PrimaryKeyJoinColumn
    private User user;

    @ManyToOne(cascade = CascadeType.REFRESH)
    @JoinColumn(name = "doctor_id")
    private Doctor doctor;

    @ManyToOne(cascade = CascadeType.REFRESH)
    @JoinColumn(name = "caregiver_id")
    private Caregiver caregiver;

    @OneToMany(cascade =  CascadeType.REFRESH, fetch = FetchType.EAGER, orphanRemoval = true)
    private List<MedicationPlan> medicationPlans;

    @ManyToMany(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
    @JoinTable(
            name = "medical_records",
            joinColumns = @JoinColumn(name = "patient_id"),
            inverseJoinColumns = @JoinColumn(name = "illness_id")

    )
    private Set<Illness> medicalRecord;

    public Patient() {
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    public Caregiver getCaregiver() {
        return caregiver;
    }

    public void setCaregiver(Caregiver caregiver) {
        this.caregiver = caregiver;
    }

    public Patient(User user, Doctor doctor, Caregiver caregiver, List<MedicationPlan> medicationPlans, Set<Illness> medicalRecord) {
        this.user = user;
        this.doctor = doctor;
        this.caregiver = caregiver;
        this.medicationPlans = medicationPlans;
        this.medicalRecord = medicalRecord;
    }

    public Patient(Long patientId, User user, Doctor doctor, Caregiver caregiver, List<MedicationPlan> medicationPlans, Set<Illness> medicalRecord) {
        this.patientId = patientId;
        this.user = user;
        this.doctor = doctor;
        this.caregiver = caregiver;
        this.medicationPlans = medicationPlans;
        this.medicalRecord = medicalRecord;
    }

    public void addMedicalPlan(MedicationPlan medicationPlan){
        medicationPlans.add(medicationPlan);
    }

    public void removeMedicalPlan(MedicationPlan medicationPlan){
        medicationPlans.remove(medicationPlan);
    }

    public void addIllness(Illness illness){
        medicalRecord.add(illness);
        illness.addPatient(this);
    }

    public void removeIllness(Illness illness){
        medicalRecord.remove(illness);
        illness.removePatient(this);
    }

    public Set<Illness> getMedicalRecord() {
        return medicalRecord;
    }

    public void setMedicalRecord(Set<Illness> medicalRecord) {
        this.medicalRecord = medicalRecord;
    }

    public List<MedicationPlan> getMedicationPlans() {
        return medicationPlans;
    }

    public void setMedicationPlans(List<MedicationPlan> medicationPlans) {
        this.medicationPlans = medicationPlans;
    }

    public Long getPatientId() {
        return patientId;
    }

    public void setPatientId(Long patientId) {
        this.patientId = patientId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Patient patient = (Patient) o;
        return patientId.equals(patient.patientId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(patientId);
    }

    @Override
    public String toString() {
        return "Patient{" +
                "patientId=" + patientId +
                ", user=" + user +
                '}';
    }
}
