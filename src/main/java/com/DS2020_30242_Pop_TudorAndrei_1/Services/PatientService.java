package com.DS2020_30242_Pop_TudorAndrei_1.Services;

import com.DS2020_30242_Pop_TudorAndrei_1.DTOs.UserDetailsDTO;
import com.DS2020_30242_Pop_TudorAndrei_1.Entity.*;
import com.DS2020_30242_Pop_TudorAndrei_1.Repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class PatientService {

    @Autowired
    private PatientRepo patientRepo;

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private MedicationPlanRepo medicationPlanRepo;

    @Autowired
    private PrescribedMedicationRepo prescribedMedicationRepo;

    @Autowired
    private CaregiverRepo caregiverRepo;

    public ResponseEntity<MedicationPlan> addMedicationPlan(Long patientId, MedicationPlan medicationPlan) {
        Patient patient = patientRepo.findByPatientId(patientId);
        patient.addMedicalPlan(medicationPlan);

        if(medicationPlan.getPrescribedMedications() != null){
            for(PrescribedMedication prescription : medicationPlan.getPrescribedMedications()){
                prescribedMedicationRepo.save(prescription);
            }
        }

        medicationPlanRepo.save(medicationPlan);

        patientRepo.save(patient);
        return new ResponseEntity<MedicationPlan>(medicationPlan, HttpStatus.ACCEPTED);

    }

    public ResponseEntity<Long> addCaregiver(Long caregiverId, Patient patient) {
        Caregiver caregiver = caregiverRepo.findByCaregiverId(caregiverId);
        caregiver.addPatient(patient);

        caregiverRepo.save(caregiver);
        patientRepo.save(patient);

        return new ResponseEntity<Long>(caregiverId, HttpStatus.ACCEPTED);
    }

    public Patient getPatientById(Long patientId) {
        return patientRepo.findByPatientId(patientId);
    }

    public List<Patient> getPatients() {
        return patientRepo.findAll();
    }

    public ResponseEntity<Long> addPrescribedMedication(Long patientId, Long medicationPlanId, PrescribedMedication prescribedMedication) {
        Patient patient = patientRepo.findByPatientId(patientId);
        MedicationPlan  medicationPlan = medicationPlanRepo.findByMedicationPlanId(medicationPlanId);
        ResponseEntity<Long> responseEntity = null;

        if(patient.getMedicationPlans().contains(medicationPlan)){
            medicationPlan.addPrescribedMedication(prescribedMedication);
            responseEntity = new ResponseEntity<Long>(patientId, HttpStatus.ACCEPTED);

            patientRepo.save(patient);
            medicationPlanRepo.save(medicationPlan);
            prescribedMedicationRepo.save(prescribedMedication);
        }else {
            responseEntity = new ResponseEntity<Long>(patientId, HttpStatus.FORBIDDEN);
        }

        return responseEntity;
    }

    public ResponseEntity<Long> removePrescribedMedication(Long patientId, Long medicationPlanId, PrescribedMedication prescribedMedication) {
        Patient patient = patientRepo.findByPatientId(patientId);
        MedicationPlan  medicationPlan = medicationPlanRepo.findByMedicationPlanId(medicationPlanId);
        ResponseEntity<Long> responseEntity = null;

        if(patient.getMedicationPlans().contains(medicationPlan)){
            medicationPlan.removePrescribedMedication(prescribedMedication);
            responseEntity = new ResponseEntity<Long>(patientId, HttpStatus.ACCEPTED);

            patientRepo.save(patient);
            medicationPlanRepo.save(medicationPlan);
            prescribedMedicationRepo.save(prescribedMedication);
        }else {
            responseEntity = new ResponseEntity<Long>(patientId, HttpStatus.FORBIDDEN);
        }

        return responseEntity;
    }


    public List<MedicationPlan> getMedicationPlans(Long patientId) {
        Patient patient = patientRepo.findByPatientId(patientId);

        return patient.getMedicationPlans();
    }

    public ResponseEntity<Long> removePatient(Long patientId) {
        Patient patient = patientRepo.findByPatientId(patientId);
        ResponseEntity<Long> responseEntity = null;

        if(patient != null){
            patientRepo.delete(patient);
            responseEntity = new ResponseEntity<Long>(patientId, HttpStatus.ACCEPTED);
        }else {
            responseEntity = new ResponseEntity<Long>(patientId, HttpStatus.FORBIDDEN);
        }

        return responseEntity;
    }

    public ResponseEntity<User> updatePatient(User user) {
        Patient patient = patientRepo.findByPatientId(user.getUserId());
        ResponseEntity<User> responseEntity = null;

        if(patient != null){
            patient.setUser(user);
            userRepo.save(user);
            patientRepo.save(patient);
            responseEntity = new ResponseEntity<User>(user, HttpStatus.ACCEPTED);
        }else {
            responseEntity = new ResponseEntity<User>(user, HttpStatus.FORBIDDEN);
        }

        return responseEntity;
    }
}
