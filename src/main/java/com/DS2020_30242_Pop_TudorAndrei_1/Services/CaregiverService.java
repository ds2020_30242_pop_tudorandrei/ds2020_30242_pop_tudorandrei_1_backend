package com.DS2020_30242_Pop_TudorAndrei_1.Services;

import com.DS2020_30242_Pop_TudorAndrei_1.Entity.Caregiver;
import com.DS2020_30242_Pop_TudorAndrei_1.Entity.Patient;
import com.DS2020_30242_Pop_TudorAndrei_1.Entity.User;
import com.DS2020_30242_Pop_TudorAndrei_1.Repositories.CaregiverRepo;
import com.DS2020_30242_Pop_TudorAndrei_1.Repositories.PatientRepo;
import com.DS2020_30242_Pop_TudorAndrei_1.Repositories.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class CaregiverService {

    @Autowired
    private CaregiverRepo caregiverRepo;

    @Autowired
    private PatientRepo patientRepo;

    @Autowired
    private UserRepo userRepo;

    public ResponseEntity<Long> addPatient(Long patientId, Long caregiverId) {
        Patient patient = patientRepo.findByPatientId(patientId);
        Caregiver caregiver = caregiverRepo.findByCaregiverId(caregiverId);
        caregiver.addPatient(patient);
        patient.setCaregiver(caregiver);

        caregiverRepo.save(caregiver);
        patientRepo.save(patient);

        return new ResponseEntity<Long>(patientId, HttpStatus.ACCEPTED);
    }

    public ResponseEntity<Long> dismissPatient(Long patientId, Long caregiverId) {
        Patient patient = patientRepo.findByPatientId(patientId);
        Caregiver caregiver = caregiverRepo.findByCaregiverId(caregiverId);
        ResponseEntity<Long> responseEntity = null;

        if(caregiver.getPatients().contains(patient)){
            caregiver.removePatient(patient);
            patient.setCaregiver(null);

            patientRepo.save(patient);
            caregiverRepo.save(caregiver);
            System.out.println("\n" + caregiver.getPatients()+ "\n");

            responseEntity = new ResponseEntity<Long>(patientId, HttpStatus.ACCEPTED);
        }else{
            responseEntity = new ResponseEntity<Long>(patientId, HttpStatus.FORBIDDEN);
        }


        return responseEntity;
    }

    public ResponseEntity<Caregiver> findCaregiver(Long caregiverId) {
        return new ResponseEntity<Caregiver>(caregiverRepo.findByCaregiverId(caregiverId), HttpStatus.OK);
    }

    public ResponseEntity<List<Caregiver>> findAllCaregivers() {
        return new ResponseEntity<List<Caregiver>>(caregiverRepo.findAll(), HttpStatus.OK);
    }

    public ResponseEntity<List<Patient>> getPatients(Long caregiverId) {
        Caregiver caregiver = caregiverRepo.findByCaregiverId(caregiverId);
        return new ResponseEntity<List<Patient>>(caregiver.getPatients(), HttpStatus.OK);
    }

    public ResponseEntity<Long> deleteCaregiver(Long caregiverId) {
        Caregiver caregiver = caregiverRepo.findByCaregiverId(caregiverId);
        ResponseEntity<Long> responseEntity = null;

        if(caregiver != null){
            caregiverRepo.delete(caregiver);
            responseEntity = new ResponseEntity<Long>(caregiverId, HttpStatus.ACCEPTED);
        }else {
            responseEntity = new ResponseEntity<Long>(caregiverId, HttpStatus.FORBIDDEN);
        }

        return responseEntity;
    }

    public ResponseEntity<User> updateCaregiver(User user) {
        Caregiver caregiver = caregiverRepo.findByCaregiverId(user.getUserId());
        ResponseEntity<User> responseEntity = null;

        if(caregiver != null){
            caregiver.setUser(user);
            userRepo.save(user);
            caregiverRepo.save(caregiver);
            responseEntity = new ResponseEntity<User>(user, HttpStatus.ACCEPTED);
        }else {
            responseEntity = new ResponseEntity<User>(user, HttpStatus.FORBIDDEN);
        }

        return responseEntity;
    }

}
