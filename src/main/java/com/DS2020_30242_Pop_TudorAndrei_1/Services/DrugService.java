package com.DS2020_30242_Pop_TudorAndrei_1.Services;

import com.DS2020_30242_Pop_TudorAndrei_1.Entity.Caregiver;
import com.DS2020_30242_Pop_TudorAndrei_1.Entity.Drug;
import com.DS2020_30242_Pop_TudorAndrei_1.Repositories.DrugRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class DrugService {

    @Autowired
    private DrugRepo drugRepo;

    public ResponseEntity<Long> insertDrug(Drug drug) {
        drugRepo.save(drug);
        return new ResponseEntity<Long>(drug.getDrugId(), HttpStatus.CREATED);
    }

    public ResponseEntity<Drug> getByDrugId(Long drugId) {
        return new ResponseEntity<Drug>(drugRepo.findByDrugId(drugId), HttpStatus.OK);
    }

    public ResponseEntity<List<Drug>> getAllDrugs() {
        return new ResponseEntity<List<Drug>>(drugRepo.findAll(), HttpStatus.OK);
    }

    public ResponseEntity<Long> deleteDrug(Long drugId) {
        Drug drug = drugRepo.findByDrugId(drugId);
        ResponseEntity<Long> responseEntity = null;

        if(drug != null){
            drugRepo.delete(drug);
            responseEntity = new ResponseEntity<Long>(drug.getDrugId(), HttpStatus.ACCEPTED);
        }else {
            responseEntity = new ResponseEntity<Long>(drug.getDrugId(), HttpStatus.FORBIDDEN);
        }

        return responseEntity;
    }

    public ResponseEntity<Long> updateDrug(Drug drug) {
        Drug drugtest = drugRepo.findByDrugId(drug.getDrugId());
        ResponseEntity<Long> responseEntity = null;

        if(drugtest != null){
            drugRepo.save(drug);
            responseEntity = new ResponseEntity<Long>(drug.getDrugId(), HttpStatus.ACCEPTED);
        }else {
            responseEntity = new ResponseEntity<Long>(drug.getDrugId(), HttpStatus.FORBIDDEN);
        }

        return responseEntity;
    }
}
