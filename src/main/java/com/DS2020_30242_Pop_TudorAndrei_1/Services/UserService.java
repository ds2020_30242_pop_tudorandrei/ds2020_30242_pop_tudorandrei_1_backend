package com.DS2020_30242_Pop_TudorAndrei_1.Services;

import com.DS2020_30242_Pop_TudorAndrei_1.Controllers.BodyRequests.LogInEntity;
import com.DS2020_30242_Pop_TudorAndrei_1.DTOs.Builders.CaregiverBuilder;
import com.DS2020_30242_Pop_TudorAndrei_1.DTOs.Builders.DoctorBuilder;
import com.DS2020_30242_Pop_TudorAndrei_1.DTOs.Builders.PatientBuilder;
import com.DS2020_30242_Pop_TudorAndrei_1.DTOs.Builders.UserBuilder;
import com.DS2020_30242_Pop_TudorAndrei_1.DTOs.UserDTO;
import com.DS2020_30242_Pop_TudorAndrei_1.DTOs.UserDetailsDTO;
import com.DS2020_30242_Pop_TudorAndrei_1.Entity.User;
import com.DS2020_30242_Pop_TudorAndrei_1.Exceptions.RoleNotSupportedException;
import com.DS2020_30242_Pop_TudorAndrei_1.Repositories.CaregiverRepo;
import com.DS2020_30242_Pop_TudorAndrei_1.Repositories.DoctorRepo;
import com.DS2020_30242_Pop_TudorAndrei_1.Repositories.PatientRepo;
import com.DS2020_30242_Pop_TudorAndrei_1.Repositories.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class UserService {

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private PatientRepo patientRepo;

    @Autowired
    private DoctorRepo doctorRepo;

    @Autowired
    private CaregiverRepo caregiverRepo;

    public ResponseEntity<User> insertUser(UserDTO userDTO, UserDetailsDTO userDetailsDTO){
        User user = UserBuilder.toEntity(userDTO, userDetailsDTO);
        //userRepo.save(user);   /// se insereaza impreuna cu doctor/patient/caregiver,
        ResponseEntity<User> response = new ResponseEntity<>(user, HttpStatus.CREATED);

        //inserts a new type of account based on user role
        try {
            insertAccountType(user);
        } catch (RoleNotSupportedException e) {
            e.printStackTrace();
            response = new ResponseEntity<>(user, HttpStatus.EXPECTATION_FAILED);
        }

        return response;
    }

    //inserts a new type of account based on user role
    private void insertAccountType(User user) throws RoleNotSupportedException {

        switch (user.getUserRole()){
            case DOCTOR:
                doctorRepo.save(DoctorBuilder.getEmptyDoctorAccount(user));
                break;
            case PATIENT:
                patientRepo.save(PatientBuilder.getEmptyPatientAccount(user));
                break;
            case CAREGIVER:
                caregiverRepo.save(CaregiverBuilder.getEmptyCaregiverAccout(user));
                break;
            default:
                throw new RoleNotSupportedException();
        }
    }

    public void updateUsername(Long id, String newUsername){
        User user = userRepo.findByUserId(id);
        user.setUserName(newUsername);
        userRepo.save(user);
    }

    public void deleteUser(Long id){
        User user = userRepo.findByUserId(id);
        userRepo.delete(user);
    }

    public List<User> findAllUsers(){
        return userRepo.findAll();
    }

    public ResponseEntity<User> checkCredentials(LogInEntity logInEntity) {
        User user = null;
        try{
            user = userRepo.findByUserName(logInEntity.getUsername());
        }catch (Exception e){
            System.out.println("Hmmmmmmmmmmmmmmmmmmmmmmmmmmmm");
        }
        ResponseEntity<User> userResponseEntity = null;

        if(user != null){

            if(user.getUserPassword().equals(logInEntity.getPassword())){
                userResponseEntity = new ResponseEntity<>(user, HttpStatus.ACCEPTED);
            }else{
                userResponseEntity = new ResponseEntity<>(new User(), HttpStatus.NOT_FOUND);
            }

        }else{
            userResponseEntity = new ResponseEntity<>(new User(), HttpStatus.NOT_FOUND);
        }

        return userResponseEntity;
    }
}
