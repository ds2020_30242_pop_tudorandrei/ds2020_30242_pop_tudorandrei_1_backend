package com.DS2020_30242_Pop_TudorAndrei_1.Services;

import com.DS2020_30242_Pop_TudorAndrei_1.Entity.Caregiver;
import com.DS2020_30242_Pop_TudorAndrei_1.Entity.Doctor;
import com.DS2020_30242_Pop_TudorAndrei_1.Entity.Patient;
import com.DS2020_30242_Pop_TudorAndrei_1.Repositories.CaregiverRepo;
import com.DS2020_30242_Pop_TudorAndrei_1.Repositories.DoctorRepo;
import com.DS2020_30242_Pop_TudorAndrei_1.Repositories.PatientRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.print.Doc;
import java.util.List;


@Service
public class DoctorService {

    @Autowired
    private DoctorRepo doctorRepo;

    @Autowired
    private PatientRepo patientRepo;

    @Autowired
    private CaregiverRepo caregiverRepo;

    public ResponseEntity<Long> hireCaregiver(Long caregiverId, Long doctorId) {
        Caregiver caregiver = caregiverRepo.findByCaregiverId(caregiverId);
        Doctor doctor = doctorRepo.findByDoctorId(doctorId);

        doctor.addCaregiver(caregiver);

        doctorRepo.save(doctor);
        caregiverRepo.save(caregiver);

        return new ResponseEntity<Long>(caregiverId, HttpStatus.ACCEPTED);
    }

    public List<Doctor> findAllDoctors() {
        return doctorRepo.findAll();
    }

    public Doctor getDoctorById(Long doctorId) {
        return doctorRepo.findByDoctorId(doctorId);
    }

    public ResponseEntity<Long> addPatient(Long doctorId, Long patientId) {
        Doctor doctor = doctorRepo.findByDoctorId(doctorId);
        Patient patient = patientRepo.findByPatientId(patientId);

        doctor.addPatient(patient);

        doctorRepo.save(doctor);
        patientRepo.save(patient);

        return new ResponseEntity<>(doctorId, HttpStatus.ACCEPTED);
    }

    public ResponseEntity<List<Patient>> getTreatedPatients(Long doctorId) {
        Doctor doctor = doctorRepo.findByDoctorId(doctorId);

        return new ResponseEntity<List<Patient>>(doctor.getPatients(), HttpStatus.OK);

    }

    public ResponseEntity<List<Caregiver>> viewHiredPatients(Long doctorId) {
        Doctor doctor = doctorRepo.findByDoctorId(doctorId);

        return new ResponseEntity<List<Caregiver>>(doctor.getCaregivers(), HttpStatus.OK);
    }

    public ResponseEntity<Long> dismissPatient(Long doctorId, Long patientId) {
        Patient patient = patientRepo.findByPatientId(patientId);
        Doctor doctor = doctorRepo.findByDoctorId(doctorId);

        ResponseEntity<Long> responseEntity = null;

        if(doctor.getPatients().contains(patient)){
            Caregiver caregiver = patient.getCaregiver();

            doctor.removePatient(patient);

            if(caregiver != null){
                caregiver.removePatient(patient);
            }
            patientRepo.save(patient);
            if(caregiver != null){
                caregiverRepo.save(caregiver);
            }
            doctorRepo.save(doctor);


            responseEntity = new ResponseEntity<Long>(patientId, HttpStatus.ACCEPTED);
        }else{
            responseEntity = new ResponseEntity<Long>(patientId, HttpStatus.FORBIDDEN);
        }

        return responseEntity;
    }

    public ResponseEntity<Long> dismissCaregiver(Long caregiverId, Long doctorId) {
        Caregiver caregiver = caregiverRepo.findByCaregiverId(caregiverId);
        Doctor doctor = doctorRepo.findByDoctorId(doctorId);

        ResponseEntity<Long> responseEntity = null;

        if(doctor.getCaregivers().contains(caregiver)){

            doctor.removeCaregiver(caregiver);

            caregiverRepo.save(caregiver);
            doctorRepo.save(doctor);


            responseEntity = new ResponseEntity<Long>(caregiverId, HttpStatus.ACCEPTED);
        }else{
            responseEntity = new ResponseEntity<Long>(caregiverId, HttpStatus.FORBIDDEN);
        }

        return responseEntity;
    }
}
