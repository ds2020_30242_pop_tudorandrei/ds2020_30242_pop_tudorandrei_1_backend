package com.DS2020_30242_Pop_TudorAndrei_1.Controllers;

import com.DS2020_30242_Pop_TudorAndrei_1.Entity.Drug;
import com.DS2020_30242_Pop_TudorAndrei_1.Services.DrugService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


//@CrossOrigin(origins = "http://localhost:3000")
@CrossOrigin(origins = "https://react-demo-ds2020-plswork.herokuapp.com")
@RestController
public class DrugController {
    @Autowired
    private DrugService drugService;

    @GetMapping(value = "drug/getById/{drugId}")
    public ResponseEntity<Drug> getDrugById(@PathVariable Long drugId){
        return drugService.getByDrugId(drugId);
    }

    @GetMapping(value = "drug/getAllDrugs")
    public ResponseEntity<List<Drug>> getAllDrugs(){
        return drugService.getAllDrugs();
    }

}
