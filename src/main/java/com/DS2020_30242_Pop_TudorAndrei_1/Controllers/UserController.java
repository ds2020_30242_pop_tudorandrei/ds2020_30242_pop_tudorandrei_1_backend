package com.DS2020_30242_Pop_TudorAndrei_1.Controllers;

import com.DS2020_30242_Pop_TudorAndrei_1.Controllers.BodyRequests.LogInEntity;
import com.DS2020_30242_Pop_TudorAndrei_1.Controllers.BodyRequests.userBodyDTOs;
import com.DS2020_30242_Pop_TudorAndrei_1.DTOs.UserDTO;
import com.DS2020_30242_Pop_TudorAndrei_1.DTOs.UserDetailsDTO;
import com.DS2020_30242_Pop_TudorAndrei_1.Entity.User;
import com.DS2020_30242_Pop_TudorAndrei_1.Services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.PermitAll;
import java.util.ArrayList;
import java.util.List;



//@CrossOrigin(origins = "http://localhost:3000")
@CrossOrigin(origins = "https://react-demo-ds2020-plswork.herokuapp.com")
@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/user/create")
    public ResponseEntity<User> addUser(@RequestBody userBodyDTOs dtoInput){
        return userService.insertUser(dtoInput.userDTO, dtoInput.userDetailsDTO);
    }

    @PostMapping("updateUsername/{id}/{newUsername}")
    public void updateUser(@PathVariable Long id, @PathVariable String newUsername){
        userService.updateUsername(id, newUsername);
    }

    @PostMapping("deleteUser/{id}")
    public void deleteUser(@PathVariable Long id){
        userService.deleteUser(id);
    }

    @GetMapping("/all/users")
    public ResponseEntity<List<User>> getAllUsers(){
        List<User> users = userService.findAllUsers();
        return new ResponseEntity<>(users, HttpStatus.OK);
    }

    @PostMapping(value = "user/logIn") //its stupid but it works
    public ResponseEntity<User> logIn(@RequestBody LogInEntity logInEntity){
        return userService.checkCredentials(logInEntity);
    }
    

}
