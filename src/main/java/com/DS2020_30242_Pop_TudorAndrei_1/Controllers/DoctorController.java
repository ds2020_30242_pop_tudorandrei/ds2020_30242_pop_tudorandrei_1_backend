package com.DS2020_30242_Pop_TudorAndrei_1.Controllers;

import com.DS2020_30242_Pop_TudorAndrei_1.DTOs.UserDetailsDTO;
import com.DS2020_30242_Pop_TudorAndrei_1.Entity.*;
import com.DS2020_30242_Pop_TudorAndrei_1.Services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

//@CrossOrigin(origins = "http://localhost:3000")
@CrossOrigin(origins = "https://react-demo-ds2020-plswork.herokuapp.com")
@RestController
public class DoctorController {

    @Autowired
    private PatientService patientService;

    @Autowired
    private CaregiverService caregiverService;

    @Autowired
    private DoctorService doctorService;

    @Autowired
    private MedicationPlanService medicationPlanService;

    @Autowired
    private PrescribedMedicationService prescribedMedicationService;

    @Autowired
    private DrugService drugService;

    //self CRUDs
    @GetMapping(value = "doctor/findAll")
    public ResponseEntity<List<Doctor>> findAllDoctors(){
        List<Doctor> doctors = doctorService.findAllDoctors();
        return new ResponseEntity<>(doctors, HttpStatus.OK);
    }

    @GetMapping(value = "doctor/getById/{doctorId}")
    public ResponseEntity<Doctor> getDoctorById(@PathVariable Long doctorId){
        return new ResponseEntity<Doctor>(doctorService.getDoctorById(doctorId), HttpStatus.OK);
    }


    @PostMapping(value = "doctor/addPatient/{patientId}/{doctorId}")
    public ResponseEntity<Long> assignDoctor(@PathVariable Long patientId, @PathVariable Long doctorId){
        return doctorService.addPatient(doctorId, patientId);
    }

    @PostMapping(value = "doctor/dismissPatient/{patientId}/{doctorId}")
    public ResponseEntity<Long> dismissPatient(@PathVariable Long patientId, @PathVariable Long doctorId){
        return doctorService.dismissPatient(doctorId, patientId);
    }

    @PostMapping(value = "doctor/addNewDrug")
    public ResponseEntity<Long> insertNewDrug(@RequestBody Drug drug){
        return drugService.insertDrug(drug);
    }

    @DeleteMapping("doctor/drug/delete/{drugId}")
    public ResponseEntity<Long> deleteDrug(@PathVariable Long drugId){
        return drugService.deleteDrug(drugId);
    }

    @PostMapping("doctor/drug/update")
    public ResponseEntity<Long> updateDrug(@RequestBody Drug drug){
        return drugService.updateDrug(drug);
    }

    //Patient CRUDS
    @DeleteMapping("doctor/patient/delete/{patientId}")
    public ResponseEntity<Long> deletePatient(@PathVariable Long patientId){
        return patientService.removePatient(patientId);
    }

    @PostMapping("doctor/patient/update")
    public ResponseEntity<User> updatePatient(@RequestBody User user){
        return patientService.updatePatient(user);
    }


    @PostMapping(value = "doctor/patient/createMedicationPlan/{patientId}")
    public ResponseEntity<MedicationPlan> addMedicationPlan(@PathVariable Long patientId, @RequestBody MedicationPlan medicationPlan){
        return patientService.addMedicationPlan(patientId, medicationPlan);
    }

    @PostMapping(value = "doctor/patient/prescribe/{patientId}/{medicationPlanId}")
    public ResponseEntity<Long> prescribeMedication(@PathVariable Long patientId,@PathVariable Long medicationPlanId, @RequestBody PrescribedMedication prescribedMedication){
        return patientService.addPrescribedMedication(patientId, medicationPlanId, prescribedMedication);
    }

    @PostMapping(value = "doctor/patient/deletePrescription/{patientId}/{medicationPlanId}")
    public ResponseEntity<Long> undoMedication(@PathVariable Long patientId,@PathVariable Long medicationPlanId, @RequestBody PrescribedMedication prescribedMedication){
        return patientService.removePrescribedMedication(patientId, medicationPlanId, prescribedMedication);
    }

    @PostMapping(value = "doctor/patient/assignCaregiver/{caregiverId}")
    public ResponseEntity<Long> assignCaregiver(@PathVariable Long caregiverId, @RequestBody Patient patient){
        return patientService.addCaregiver(caregiverId, patient);
    }

    @GetMapping(value = "doctor/patient/getTreatedPatients/{doctorId}")
    public ResponseEntity<List<Patient>> viewTreatedPatients(@PathVariable Long doctorId){
        return doctorService.getTreatedPatients(doctorId);
    }

    //Caregiver CRUDS
    @DeleteMapping("doctor/caregiver/delete/{caregiverId}")
    public ResponseEntity<Long> deleteCaregiver(@PathVariable Long caregiverId){
        return caregiverService.deleteCaregiver(caregiverId);
    }

    @PostMapping("doctor/caregiver/update")
    public ResponseEntity<User> updateCaregiver(@RequestBody User user){
        return caregiverService.updateCaregiver(user);
    }

    @PostMapping(value = "doctor/caregiver/hireCaregiver/{caregiverId}/{doctorId}")
    public ResponseEntity<Long> hireCaregiver(@PathVariable Long caregiverId,@PathVariable Long doctorId){
        return doctorService.hireCaregiver(caregiverId, doctorId);
    }


    @PostMapping(value = "doctor/caregiver/assignPatient/{patientId}/{caregiverId}")
    public ResponseEntity<Long> assignPatient(@PathVariable Long patientId, @PathVariable Long caregiverId) {
        return caregiverService.addPatient(patientId, caregiverId);
    }

    @DeleteMapping(value = "doctor/caregiver/dismissPatient/{patientId}/{caregiverId}")
    public ResponseEntity<Long> dismissCaregiverPatient(@PathVariable Long patientId, @PathVariable Long caregiverId) {
        return caregiverService.dismissPatient(patientId, caregiverId);
    }

    @PostMapping(value = "doctor/caregiver/dismiss/{caregiverId}/{doctorId}")
    public ResponseEntity<Long> dismissCaregiver(@PathVariable Long caregiverId, @PathVariable Long doctorId) {
        return doctorService.dismissCaregiver(caregiverId, doctorId);
    }

    @GetMapping(value = "doctor/caregiver/getHiredCaregivers/{doctorId}")
    public ResponseEntity<List<Caregiver>> getHiredCaregivers(@PathVariable Long doctorId){
        return doctorService.viewHiredPatients(doctorId);
    }

}
