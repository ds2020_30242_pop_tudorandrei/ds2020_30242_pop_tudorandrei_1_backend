package com.DS2020_30242_Pop_TudorAndrei_1.Controllers;

import com.DS2020_30242_Pop_TudorAndrei_1.Entity.Caregiver;
import com.DS2020_30242_Pop_TudorAndrei_1.Entity.Patient;
import com.DS2020_30242_Pop_TudorAndrei_1.Services.CaregiverService;
import lombok.experimental.PackagePrivate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

//@CrossOrigin(origins = "http://localhost:3000")
@CrossOrigin(origins = "https://react-demo-ds2020-plswork.herokuapp.com")
@RestController
public class CaregiverController {

    @Autowired
    private CaregiverService caregiverService;


    @GetMapping(value = "caregiver/findById/{caregiverId}")
    public ResponseEntity<Caregiver> findById(@PathVariable Long caregiverId){
        return caregiverService.findCaregiver(caregiverId);
    }

    @GetMapping(value = "caregiver/assignedPatients/{caregiverId}")
    public ResponseEntity<List<Patient>> findAssignedPatientsById(@PathVariable Long caregiverId){
        return caregiverService.getPatients(caregiverId);
    }

    @GetMapping(value = "caregiver/getAll")
    public ResponseEntity<List<Caregiver>> getCaregivers(){
        return caregiverService.findAllCaregivers();
    }


}
