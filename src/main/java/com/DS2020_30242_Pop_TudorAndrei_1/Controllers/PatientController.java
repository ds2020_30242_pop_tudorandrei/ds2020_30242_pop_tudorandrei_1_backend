package com.DS2020_30242_Pop_TudorAndrei_1.Controllers;

import com.DS2020_30242_Pop_TudorAndrei_1.Entity.MedicationPlan;
import com.DS2020_30242_Pop_TudorAndrei_1.Entity.Patient;
import com.DS2020_30242_Pop_TudorAndrei_1.Services.CaregiverService;
import com.DS2020_30242_Pop_TudorAndrei_1.Services.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.security.PermitAll;
import java.util.List;

//@CrossOrigin(origins = "http://localhost:3000")
@CrossOrigin(origins = "https://react-demo-ds2020-plswork.herokuapp.com")
@RestController
public class PatientController {

    @Autowired
    private PatientService patientService;

    @GetMapping(value = "patient/getById/{patientId}")
    public ResponseEntity<Patient> getPatientById(@PathVariable Long patientId){
        return new ResponseEntity<Patient>(patientService.getPatientById(patientId), HttpStatus.OK);
    }

    @GetMapping(value = "patient/getAll")
    public ResponseEntity<List<Patient>> getPatients(){
        return new ResponseEntity<>(patientService.getPatients(), HttpStatus.OK);
    }

    @GetMapping(value = "patient/getMedicationPlans/{patientId}")
    public ResponseEntity<List<MedicationPlan>> getMedicationPlans(@PathVariable Long patientId){
        return new ResponseEntity<List<MedicationPlan>>(patientService.getMedicationPlans(patientId), HttpStatus.OK);
    }

}
