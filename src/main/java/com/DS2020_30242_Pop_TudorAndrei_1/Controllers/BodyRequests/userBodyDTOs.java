package com.DS2020_30242_Pop_TudorAndrei_1.Controllers.BodyRequests;

import com.DS2020_30242_Pop_TudorAndrei_1.DTOs.UserDTO;
import com.DS2020_30242_Pop_TudorAndrei_1.DTOs.UserDetailsDTO;

public class userBodyDTOs {

    public UserDTO userDTO;
    public UserDetailsDTO userDetailsDTO;

    public userBodyDTOs() {
    }

    public userBodyDTOs(UserDTO userDTO, UserDetailsDTO userDetailsDTO) {
        this.userDTO = userDTO;
        this.userDetailsDTO = userDetailsDTO;
    }

    public UserDTO getUserDTO() {
        return userDTO;
    }

    public void setUserDTO(UserDTO userDTO) {
        this.userDTO = userDTO;
    }

    public UserDetailsDTO getUserDetailsDTO() {
        return userDetailsDTO;
    }

    public void setUserDetailsDTO(UserDetailsDTO userDetailsDTO) {
        this.userDetailsDTO = userDetailsDTO;
    }
}
