package com.DS2020_30242_Pop_TudorAndrei_1.Repositories;

import com.DS2020_30242_Pop_TudorAndrei_1.Entity.Drug;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;



@Repository
public interface DrugRepo extends JpaRepository<Drug, Long> {
    Drug findByDrugId(Long drugId);
}
