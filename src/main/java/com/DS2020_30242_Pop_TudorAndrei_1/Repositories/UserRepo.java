package com.DS2020_30242_Pop_TudorAndrei_1.Repositories;
import com.DS2020_30242_Pop_TudorAndrei_1.Entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;



@Repository
public interface UserRepo extends JpaRepository<User, Long> {

    public User findByUserId(Long id);

    public void deleteByUserId(Long id);


    User findByUserName(String username);
}
