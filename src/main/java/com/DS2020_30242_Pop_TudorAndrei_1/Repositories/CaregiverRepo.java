package com.DS2020_30242_Pop_TudorAndrei_1.Repositories;

import com.DS2020_30242_Pop_TudorAndrei_1.Entity.Caregiver;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;



@Repository
public interface CaregiverRepo extends JpaRepository<Caregiver, Long> {
    Caregiver findByCaregiverId(Long caregiverId);
}
