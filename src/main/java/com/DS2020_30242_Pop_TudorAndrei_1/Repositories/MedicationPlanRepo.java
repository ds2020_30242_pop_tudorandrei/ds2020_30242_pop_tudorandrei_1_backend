package com.DS2020_30242_Pop_TudorAndrei_1.Repositories;

import com.DS2020_30242_Pop_TudorAndrei_1.Entity.MedicationPlan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;



@Repository
public interface MedicationPlanRepo extends JpaRepository<MedicationPlan, Long> {
    MedicationPlan findByMedicationPlanId(Long medicationPlanId);
}
