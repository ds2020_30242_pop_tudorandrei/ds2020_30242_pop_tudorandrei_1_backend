package com.DS2020_30242_Pop_TudorAndrei_1.Repositories;

import com.DS2020_30242_Pop_TudorAndrei_1.Entity.Patient;
import com.DS2020_30242_Pop_TudorAndrei_1.Entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface PatientRepo extends JpaRepository<Patient, Long> {
    Patient findByPatientId(Long patientId);

}
