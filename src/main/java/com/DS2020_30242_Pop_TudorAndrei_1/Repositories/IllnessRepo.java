package com.DS2020_30242_Pop_TudorAndrei_1.Repositories;

import com.DS2020_30242_Pop_TudorAndrei_1.Entity.Illness;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;



@Repository
public interface IllnessRepo extends JpaRepository<Illness, Long> {
}
