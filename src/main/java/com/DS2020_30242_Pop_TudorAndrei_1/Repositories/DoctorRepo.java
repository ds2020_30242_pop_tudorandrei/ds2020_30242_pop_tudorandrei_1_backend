package com.DS2020_30242_Pop_TudorAndrei_1.Repositories;

import com.DS2020_30242_Pop_TudorAndrei_1.Entity.Doctor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.print.Doc;


@Repository
public interface DoctorRepo extends JpaRepository<Doctor, Long> {
    Doctor findByDoctorId(Long doctorId);
}
